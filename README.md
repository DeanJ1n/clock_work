# Clock Worker!
***
## Info
Clock worker is a 2.5D puzzle game make with unity, the game is originally made for game jam monthly for Game Dev Club of Mcgill University.
***
## Installation
You can download the game [here](https://yutian-jin.itch.io/clock-worker). Mac and Windows versions will be available, unzip to play.
***
## Play Guide
Use left and right mouse button to tweak the key on the left of the screen to control different clockwork on the right. To win the level you will need to manage the clockworks to deliver the balls to destination pipe.