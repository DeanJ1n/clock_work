﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    //design levers
    [SerializeField] Text wintext;
    [SerializeField] Text countText;
    [SerializeField] Text levelNum;
    [SerializeField] float delay;
    [SerializeField] AudioClip winning;


    //private fields
    private bool prepare = true;
    private int count = 0;
    private int point = 0;
    private bool restarting = false;
    private AudioSource source;

	// Use this for initialization
	void Start () {
        wintext.text = "";
        countText.text = "Point: " + point + "/" + count;
        source = GetComponent<AudioSource>();
        levelNum.text = "level " + SceneManager.GetActiveScene().buildIndex;
	}

    //here is the getter for prepare
    public bool GetPrepare()
    {
        return prepare;
    }

    //end the preparation when the button is clicked
    public void EndPrepare()
    {
        prepare = false;
    }

    //increase the count of points at the beginning of game, this is called by outpipes
    public void CountPoint(int countPoint)
    {
        count = count + countPoint;

        //update the text
        countText.text = "Point: " + point + "/" + count;

    }

    //increase the player's point when a target is successfully got into in-pipe
    public void IncreasePoint()
    {
        point++;

        //update the text
        countText.text = "Point: " + point + "/" + count;

        //if all points are collected, we end the level by presenting the player a win message
        if (point == count)
        {
            wintext.text = "YOU WON!";
            source.PlayOneShot(winning);
            Invoke("NextLevel", delay);
        }
    }

    //reload scene if the player clicks the button
    private void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    //load next scene when the player won
    public void LoadNextScene()
    {
        Invoke("NextLevel", delay/2);
        wintext.text = "LOADING...";
        restarting = true;
    }

    private void NextLevel()
    {
        SceneManager.LoadScene((SceneManager.GetActiveScene().buildIndex + 1) % SceneManager.sceneCountInBuildSettings);
    }

    //to delay reload scene
    public void Reload()
    {
        wintext.text = "RESTARTING...";
        restarting = true;
        Invoke("Replay", delay);
    }

    //this will be called by the fall detector to show that the player lost
    public void FailGame()
    {
        if (!restarting)
        {
            wintext.text = "YOU LOST...";
            Invoke("Replay", delay);
        }
    }

    public void ExitGame()
    {
        //exit the game when pressed on button
        Application.Quit();
    }

    public void LoadLevel(int levelIndex)
    {
        SceneManager.LoadScene(levelIndex);
    }

    public void DelayLoadLevel(int levelIndex)
    {
        Invoke("LoadLevel", delay/2);
    }
}
