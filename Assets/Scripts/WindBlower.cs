﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindBlower : MonoBehaviour {

    //design levers
    [SerializeField] float windLevel;

    //private fields
    private const float ROTATE_MULTIPLIER = 12f;
    private const float WIND_LEVEL_MULTIPLIER = 20f;
	
	// Update is called once per frame
	void Update () {
        //rotate the fan every frame
        transform.RotateAround(transform.position, transform.right, Time.deltaTime * windLevel * ROTATE_MULTIPLIER);
    }

    //to blow the object away when on trigger
    private void OnTriggerStay(Collider other)
    {
        //check if the triggered object is a delivery
        if(other.gameObject.tag == "Delivery")
        {
            //blow the delivery away
            other.gameObject.GetComponent<Rigidbody>().AddForce(transform.right * windLevel * Time.deltaTime * WIND_LEVEL_MULTIPLIER);
        }
    }

    //getter for wind level
    public float GetWindLevel()
    {
        return windLevel;
    }

    //setter for wind level
    public void SetWindLevel(float level)
    {
        windLevel = level;
    }
}
