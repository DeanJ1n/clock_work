﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutPipe : MonoBehaviour {

    //design levers
    [SerializeField] GameObject delivery;
    [SerializeField] int numOfDelivery;
    [SerializeField] float delay;
    [SerializeField] float initialForce;

    //private field
    private LevelManager lm;
    private bool generated = false;

	// Use this for initialization
	void Start () {

        //get the level manager and count deliveries
        lm = FindObjectOfType<LevelManager>();
        lm.CountPoint(numOfDelivery);
	}
	
	// Update is called once per frame
	void Update () {
        if (!lm.GetPrepare() && !generated)
        {
            Debug.Log("start generate");
            generated = true;
            Generate();
        }
	}

    //here is the method to generate deliveries
    void Generate()
    {
        StartCoroutine(HelperGenerate());
    }

    IEnumerator HelperGenerate()
    {
        for(int i = 0; i < numOfDelivery; i++)
        {
            Instantiate(delivery, transform.position, transform.rotation).GetComponent<Rigidbody>().AddForce(transform.up * initialForce);
            yield return new WaitForSeconds(delay);
        }
    }

}
