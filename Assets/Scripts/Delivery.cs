﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Delivery : MonoBehaviour {

    //design levers
    [SerializeField] AudioClip[] colliding;

    //private fields
    private AudioSource source;
    private bool delivered = false;

	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
	}

    public void NotifyDelivery()
    {
        delivered = true;
        this.gameObject.tag = "Untagged";
    }

    private void OnCollisionEnter(Collision collision)
    {
        //when colliding with other object, play effect
        if (!delivered)
        {
            source.PlayOneShot(colliding[Random.Range(0, colliding.Length)]);
        }
    }
}
