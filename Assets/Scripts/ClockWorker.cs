﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ClockWorker : MonoBehaviour {

    bool tweak = false;

    //virtual methods for child class to tweak positive and negative
    abstract public bool TweakPositive();
    abstract public bool TweakNegative();

    //here is a method for tweaker to call when adjusting the movement of this certain part
    public void Tweaking(bool isTweaking)
    {
        //flip the tweaking indicator when clicked
        tweak = isTweaking;
    }

    //here is the getter for tweak
    public bool GetTweak()
    {
        return tweak;
    }
}
