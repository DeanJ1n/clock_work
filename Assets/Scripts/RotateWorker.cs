﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateWorker : ClockWorker {

    //designer levers
    [SerializeField] float rateOfRotation;
    [SerializeField] float boundLow;
    [SerializeField] float boundHigh;

    //private fields
    private float rotationZ;
    private float startRotZ;
    private int orien = 1;

	// Use this for initialization
	void Start () {
        startRotZ = transform.localEulerAngles.z;
	}

    private void Tweak()
    {
        rotationZ += Time.deltaTime * rateOfRotation * orien;
        if (rotationZ >= boundHigh)
        {
            //flip the direction of rotation when reached the maximum
            orien = -1;
        }else if(rotationZ <= boundLow)
        {
            //flip the direction of rotation when reached the maximum
            orien = 1;
        }

        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, startRotZ - rotationZ);
    }

    public override bool TweakPositive()
    {
        //rotate the clockwork clockwisely
        rotationZ += Time.deltaTime * rateOfRotation;

        //check if rotationZ is beyond maximum
        if(rotationZ > boundHigh)
        {
            rotationZ = boundHigh;
            return false;
        }

        //if the rotation is fine, set it to the clock worker
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, startRotZ - rotationZ);

        //return true if everything are fine
        return true;
    }

    public override bool TweakNegative()
    {
        //rotate the clockwork clockwisely
        rotationZ -= Time.deltaTime * rateOfRotation;

        //check if rotationZ is beyond maximum
        if (rotationZ < boundLow)
        {
            rotationZ = boundLow;
            return false;
        }

        //if the rotation is fine, set it to the clock worker
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, startRotZ - rotationZ);

        //return true if everything are fine
        return true;
    }
}
