﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSoundEffect : MonoBehaviour {

    //design levers
    [SerializeField] AudioClip buttonClip;

    //private fields
    private AudioSource source;

	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
	}
	
	public void PlayButtonEffect()
    {
        source.PlayOneShot(buttonClip);
    }
}
