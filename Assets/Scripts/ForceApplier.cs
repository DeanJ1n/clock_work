﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceApplier : MonoBehaviour {

    //design levers
    [SerializeField] float forceRatio;
    [SerializeField] AudioClip thrustEffect;

    //private fields
    private AudioSource source;

    private void Start()
    {
        source = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Delivery")
        {
            //play sound effect
            source.PlayOneShot(thrustEffect);

            //apply extra force to the direction of the object's movement
            Rigidbody tempRD = other.gameObject.GetComponent<Rigidbody>();
            tempRD.AddForce(forceRatio * tempRD.velocity.normalized);
        }
    }

}
