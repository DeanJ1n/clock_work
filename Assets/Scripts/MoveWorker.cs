﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWorker : ClockWorker {

    //Design levers
    [SerializeField] Vector3 movement;
    [SerializeField] Vector3 upBound;
    [SerializeField] Vector3 lowBound;
    [SerializeField] float rateOfMovement;

    //private fields
    private Vector3 initialPosition;
    private Vector3 tempMove;
    private Vector3 preTemp;

	// Use this for initialization
	void Start () {
        initialPosition = transform.position;
	}

    public override bool TweakPositive()
    {
        //get the temp movement
        preTemp = tempMove;
        tempMove = tempMove + movement * Time.deltaTime * rateOfMovement;

        //check if the temp movement is beyond the up bound
        if(tempMove.x > upBound.x || tempMove.y > upBound.y || tempMove.z > upBound.z)
        {
            //regulate the movement to previous value before changes
            tempMove = preTemp;
            return false;
        }

        //if there is nothing wrong, set the movement and return true
        transform.position = tempMove + initialPosition;
        return true;
    }

    public override bool TweakNegative()
    {
        //get the temp movement
        preTemp = tempMove;
        tempMove = tempMove - movement * Time.deltaTime * rateOfMovement;

        //check if the temp movement is below the low bound
        if (tempMove.x < lowBound.x || tempMove.y < lowBound.y || tempMove.z < lowBound.z)
        {
            //regulate the movement to previous value before changes
            tempMove = preTemp;
            return false;
        }

        //if there is nothing wrong, set the movement and return true
        transform.position = tempMove + initialPosition;
        return true;
    }
}
